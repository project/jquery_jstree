OVERVIEW
--------
This jquery_jstree module is a wrapper module for jsTree jQuery library. Module
integrates jsTree to Drupal so that it can be used in variety of ways. Built-in
sub module provides widget for term references. jsTree can be used in custom
forms as any Form API element.

DEPENDENCIES
------------
 - libraries
 - jquery_update (jquery version 1.9.0 or higher is required)
 - jsTree library

Place "dist/" directory of jsTree package to path sites/all/libraries/jstree or
use Drush make file that can be found from module directory.

SUB MODULES
-----------
jstree_taxonomy module provides field widget for taxonomy term reference fields.

jstree_example is just an example module demonstrating how jstree elements can
be set up in custom modules.
